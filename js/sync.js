/**
 * @file
 * Js lib used for diffrence view.
 */

(function ($) {
  'use strict';

/**
 * Attaching function onload event.
 */
  window.onload = function () {
    sync_text_areas(document.forms.formName.elements.baseText, document.forms.formName.elements.newText);
  };

  /**
   * Function is created to sync scrollbar's scroll.
   * @param {string} textarea1
   *   Base textarea name.
   * @param {string} textarea2
   *   Old version textarea name.
   */
  function sync_text_areas(textarea1, textarea2) {
    // Javascript onscoll event.
    textarea1.onscroll = function (evt) {
      var scrollflag = document.getElementById('scrollflag').checked;
      if (scrollflag) {
        textarea2.scrollTop = this.scrollTop;
      }
    };
    textarea2.onscroll = function (evt) {
      var scrollflag = document.getElementById('scrollflag').checked;
      if (scrollflag) {
        textarea1.scrollTop = this.scrollTop;
      }
    };
  }
})(jQuery);
