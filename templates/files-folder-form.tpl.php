<?php

/**
 * @file
 * Template file for compare page.
 */
?>
<b><?php print t("You are compairing file @filename", array("@filename" => $data['file_name']));?></b>
<form action="" onsubmit="return false;" name="formName">
<table class="files-version-table">
<thead>
  <tr>
    <th><?php print t("Current Drupal instance file."); ?></th>
    <th><?php print t("Version of <b> @version </b> File.", array("@version" => $data['version'])); ?></th>
  </tr>
</thead>
<tbody>
<tr class="contaxt-size-tr">
  <td colspan="2">
    <table>
    <tr>
        <td><strong><?php print t("Context Size (Optional):") ?></strong></td>
        <td><input type="text" id="contextSize" value="" placeholder="<?php print t("Please enter context size you want."); ?>" /></td>
          <td><strong><?php print t("Scroll Both Files.") ?></strong></td>
        <td><input type="checkbox" id="scrollflag" name="scrollflag" checked/></td>
      </tr>
    </table>
  </td>
</tr>
  <tr>
    <td>
    <textarea id="baseText" readonly="true">
        <?php print $data['new_file']; ?>
    </textarea>
  </td>
    <td>
      <textarea id="newText" readonly="true">
        <?php print $data['old_file']; ?>
      </textarea>
    </td>
  </tr>
</tbody>
</table>
<div class="viewType">
  <table>
  <tr>
  <td><label for="sidebyside"><?php print t("Show Side By Side Difference");?></label></td>
  <td><input type="radio" name="_viewtype" id="sidebyside" onclick="diffUsingJS(0);" /></td>
  <td><label for="inline"><?php print t("Show Inline Difference");?></label></td>
  <td><input type="radio" name="_viewtype" id="inline" onclick="diffUsingJS(1);" /></td>
  </tr>
  </table>
</div>
</form>
<div id="diffoutput"></div>
