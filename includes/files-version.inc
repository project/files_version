<?php

/**
 * @file
 * Helper file for the files version module.
 */

/**
 * Responsiable for return html of table list and result of file difference.
 *
 * @global type $base_url
 *
 * @return string
 *   Returns theme table with result.
 */
function files_version_files_folder_show_all() {
  global $base_url;
  $theme = array();
  $headers = array();
  $empty_row_flag = 0;
  $directory = DRUPAL_ROOT;
  $is_own_directory = FALSE;
  $empty_message = t("You still not created any version. Please create it.");
  // Define table header.
  $headers['name'] = t("Name");
  $headers['mime_type'] = t("Mime Type");
  // Validation is require for check file exist or not.
  $file_path_from_url = isset($_GET['file_path']) ? $_GET['file_path'] : "";
  // Adding slash for preventing wrong input from url.
  $real_path = realpath(".") . "/";
  // Check for user is broswing own directory.
  if ((substr($file_path_from_url, 0, strlen($real_path)) == $real_path) || $file_path_from_url == "") {
    $is_own_directory = TRUE;
  }
  // If user is at own directory then execute else return access denied.
  if ($is_own_directory) {
    if (isset($file_path_from_url) && !empty($file_path_from_url) && strlen($file_path_from_url) > 0) {
      $directory = $file_path_from_url;
    }
    if (is_dir($directory)) {
      // Ignore list.
      $ignore = array(".", "..");
      $files = scandir($directory);
      // Process each files and folder for version list.
      foreach ($files as $doc) {
        if (!in_array($doc, $ignore)) {
          $file_path = $directory . "/" . $doc;
          // Check for file.
          if (!is_dir($file_path)) {
            $row = array();
            $file_db_data = files_version_get_all_versions_of_file($file_path);
            foreach ($file_db_data as $db_data) {
              // Create icon path.
              $icon_path = $base_url . "/" . drupal_get_path("module", "files_version") . "/images/file.gif";
              // Create icon image markup.
              $icon = "<img src='" . $icon_path . "'> ";
              // Create name of file prefix by icon.
              $row['name'] = $icon . $doc;
              // Get mime type of file.
              $row['mime_type'] = files_version_get_file_mime($file_path);
              if (strpos($row['mime_type'], "text") !== FALSE) {
                // Link to show as table difference.
                $difference_link = l(t("Compare version"), FILES_VERSION_DRUPAL_FILE_VERSION_URL_BROWSER_URL, array("query" => array("file_path" => $file_path, "from_version" => $db_data['created'])));
              }
              else {
                // As this is not a mime type text.
                // So we will not give link to compare.
                $difference_link = t("Not a text mime type");
              }
              // Full link which will display in every row.
              $row[$db_data['created']] = $difference_link;
              // Set table header.
              $headers[$db_data['created']] = $db_data['created'];
            }
            // Assign entire row in array.
            $rows[] = $row;
          }
          // Check for directory.
          else {
            $row = array();
            $file_db_data = files_version_get_all_versions_of_file($file_path);
            foreach ($file_db_data as $db_data) {
              // Create icon path.
              $icon_path = $base_url . "/" . drupal_get_path("module", "files_version") . "/images/folder.gif";
              // Create icon image markup.
              $icon = "<img src='" . $icon_path . "'> ";
              // Create name of file prefix by icon.
              $row['name'] = $icon . l($doc, FILES_VERSION_DRUPAL_FILE_VERSION_URL_BROWSER_URL, array("query" => array("file_path" => $file_path)));
              // Get mime type of file.
              $row['mime_type'] = files_version_get_file_mime($file_path);
              // As this is a directory so that N/A value is provided.
              $row[$db_data['created']] = t("N/A");
              // Set table header.
              $headers[$db_data['created']] = $db_data['created'];
            }
            // Assign entire row in array.
            $rows[] = $row;
          }
        }
      }
      foreach ($rows as $count_row) {
        if (!empty($count_row)) {
          $empty_row_flag += 1;
        }
      }
      if (!$empty_row_flag) {
        $rows = array();
      }
      $theme = theme('table', array(
        'header' => $headers,
        'rows' => $rows,
        'empty' => $empty_message,
      )
      );
      return $theme;
    }
    else {
      // If file location is provided then check weather file exist or not.
      // If file exist then proceed otherwise return access denied page.
      if (file_exists($directory)) {
        $line = "";
        $code = "";
        $file = fopen($directory, "r");
        // Filter input because value is directly taken from url.
        $get_from_version = filter_input(INPUT_POST, 'from_version');
        $from_version = files_version_validate_version($get_from_version);
        // Check of file.
        while (!feof($file)) {
          // Read file line by line.
          $line = fgets($file);
          // Append lines in code variable.
          $code .= highlight_string($line, TRUE);
        }
        // Close file.
        fclose($file);
        // Get file saved into database.
        $file_data_query = db_select("files_version", "dg");
        $file_data_query->fields("dg", array("content"));
        $file_data_query->condition("dg.file", $directory);
        // If version is set then apply condition.
        if ($from_version) {
          $file_data_query->condition("dg.created", $from_version);
        }
        $file_data_query->orderBy("created", "DESC");
        $file_data = $file_data_query->execute()->fetchField();
        // Create a temp file then compare.
        $temp = tmpfile();
        $meta_datas = stream_get_meta_data($temp);
        $temp_version_file = $meta_datas['uri'];
        fwrite($temp, $file_data);
        rewind($temp);
        drupal_add_css(drupal_get_path('module', 'files_version') . '/css/files_version.css');
        return files_version_get_file_difference($directory, $temp_version_file);
      }
      else {
        drupal_set_message(t("File which you are looking for is not found."), "error");
        return MENU_ACCESS_DENIED;
      }
    }
  }
  else {
    drupal_set_message(t("You are not allowed to access other then realpath of your drupal installation."), "error");
    return MENU_ACCESS_DENIED;
  }
}

/**
 * Function is resonsiable to scan directory in push result into array.
 *
 * @param string $dir
 *   Directory path.
 * @param array $results
 *   Result of scaned directory.
 *
 * @return array
 *   Gives array of files path.
 */
function files_version_get_dir_contents($dir, array &$results = array()) {
  $files = scandir($dir);
  foreach ($files as $value) {
    $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
    if (!is_dir($path)) {
      $results[] = $path;
    }
    elseif ($value != "." && $value != "..") {
      files_version_get_dir_contents($path, $results);
      $results[] = $path;
    }
  }

  return $results;
}

/**
 * Function makes database entry for given file w.r.t request time.
 *
 * @param string $file
 *   File real url should be pass.
 */
function files_version_maintain_file($file) {
  // Check if file size is zero then not need to insert into db.
  $file_size = filesize($file);
  if ($file_size) {
    $fp = fopen($file, 'r');
    $content = fread($fp, $file_size);
    // Insert files data into table.
    db_insert('files_version')
      ->fields(array(
        'file' => $file,
        'size' => $file_size,
        'content' => $content,
        'created' => variable_get('batch_request_time'),
      ))
      ->execute();
    // Get latest files id.
    $get_latest_file = db_select('files_version', 'dg')
      ->fields('dg', array('id'))
      ->condition('dg.file', $file)
      ->orderBy('dg.created', 'DESC')
      ->range(0, FILES_VERSION_DRUPAL_FILE_VERSION_BACKUP_LIMIT);
    $files_id = $get_latest_file->execute();
    // Get not in condition that is id of latest three files.
    while ($record = $files_id->fetchField()) {
      $not_in[] = $record;
    }
    // Now delete old files entry.
    db_delete('files_version')
      ->condition("id", array($not_in), "NOT IN")
      ->condition("file", $file)
      ->execute();
  }
}

/**
 * Function returns file content if found any version is saved in database.
 *
 * @param string $file
 *   File realpath.
 *
 * @return array
 *   Array of versions that can use further.
 */
function files_version_get_all_versions_of_file($file) {
  if (!empty($file)) {
    $file_version_query = db_select("files_version", "fv")
      ->fields("fv", array('created'))
      ->condition("fv.file", $file)
      ->orderBy("fv.created", "DESC");
    return $file_version_query->execute()->fetchAll(PDO::FETCH_ASSOC);
  }
  else {
    return array();
  }
}

/**
 * Function returns versions.
 *
 * @return array
 *   Distinct versions of all files.
 */
function files_version_get_distinct_created_time() {
  $file_version_query = db_select('files_version', 'fv');
  $file_version_query->AddExpression('distinct fv.created', 'created');
  $file_version_query->orderBy('fv.created', 'DESC');
  $data = $file_version_query->execute()->fetchAll(PDO::FETCH_ASSOC);
  return $data;
}

/**
 * Function returns mime type of given file path input.
 *
 * @param string $path
 *   Path of file.
 *
 * @return string
 *   Gives mime type of file if found otherwise returns N/A.
 */
function files_version_get_file_mime($path) {
  if (!empty($path)) {
    $file_info = new finfo(FILEINFO_MIME_TYPE);
    return $file_info->file($path);
  }
  else {
    return t("N/A");
  }
}

/**
 * Validate the version for version should be numaric and greather then 0.
 *
 * @param int $get_from_version
 *   Value of version you want to kepp in database.
 *
 * @return int
 *   Returns the version or 0.
 */
function files_version_validate_version($get_from_version) {
  // Check for integer and greter then zero.
  if (isset($get_from_version) && is_numeric($get_from_version) && $get_from_version > 0) {
    return $get_from_version;
  }
  return 0;
}

/**
 * Get saved bakcup into array crosspending to version.
 *
 * @param int $version
 *   Version.
 *
 * @return array
 *   Get array of version used for validation.
 */
function files_version_get_backup_of_version($version) {
  if ($version) {
    $backup_query = db_select('files_version', 'fv')
      ->fields('fv', array('file', 'content'))
      ->condition('fv.created', $version);
    return $backup_query->execute()->fetchAll(PDO::FETCH_ASSOC);
  }
}

/**
 * Creates bakcup folder.
 *
 * @param int $version
 *   Version for which you want to create backup.
 */
function files_version_save_backup_of_version($version) {
  $files_version_public_directory = "";
  if ($version) {
    $versions_info = files_version_get_backup_of_version($version);
    $version_count = 0;
    if (!empty($versions_info)) {
      foreach ($versions_info as $version_data) {
        $file_info = pathinfo($version_data['file']);
        $dirname = $file_info['dirname'];
        $basename = $file_info['basename'];
        $file_bakcup_path = ($dirname) ? str_replace(DRUPAL_ROOT, "", $dirname) : $dirname;
        $files_version_public_directory = "public://files_version/backup/$version/$file_bakcup_path";
        $version_public_directory = "public://files_version/backup/";
        file_prepare_directory($files_version_public_directory, FILE_CREATE_DIRECTORY);
        // Replace file if already exists.
        $is_saved = file_unmanaged_save_data($version_data['content'], $files_version_public_directory . "/" . $basename, FILE_EXISTS_REPLACE);
        if ($is_saved) {
          $version_count++;
        }
      }
      $zip_path = drupal_realpath($version_public_directory) . "$version.zip";
      $is_zip_created = files_version_create_zip_of_backup(drupal_realpath($files_version_public_directory), $zip_path);
      if ($is_zip_created) {
        $backup_file_name = "backup$version.zip";
        drupal_goto(FILES_VERSION_DRUPAL_FILE_VERSION_URL_BACKUP_URL . "/" . $backup_file_name . "/" . time() . "$version.zip");
      }
      return $version_count;
    }
  }
  return FALSE;
}

/**
 * Creates zip file on desired location from a source.
 *
 * @param string $source
 *   Source path.
 * @param string $destination
 *   Destination where you want to save your backup.
 *
 * @return bool
 *   Boolen value if zip is created or not.
 */
function files_version_create_zip_of_backup($source, $destination) {
  if (!extension_loaded('zip') || !file_exists($source)) {
    drupal_set_message(check_plain(t("zip extension is not found. Unable to created zip. But your backup has been created in your public://files/files_version/backup directory.")), 'error');
    return FALSE;
  }
  $archive = $destination;
  $zip = new ZipArchive();
  if (!$zip->open($archive, ZIPARCHIVE::CREATE)) {
    return FALSE;
  }
  $source = str_replace('\\', '/', realpath($source));
  if (is_dir($source) === TRUE) {
    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
    foreach ($files as $file) {
      $file = str_replace('\\', '/', $file);
      // Ignore "." and ".." folders.
      if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..'))) {
        continue;
      }
      $file = realpath($file);
      if (is_dir($file) === TRUE) {
        $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
      }
      elseif (is_file($file) === TRUE) {
        $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
      }
    }
  }
  elseif (is_file($source) === TRUE) {
    $zip->addFromString(basename($source), file_get_contents($source));
  }
  $zip->close();
  return TRUE;
}

/**
 * Download zip file.
 *
 * @param string $file_uri
 *   Uri of zip file should be given.
 * @param string $filename
 *   Name is given for downloaded file.
 */
function files_version_backup_download($file_uri, $filename) {
  files_version_file_transfer("public://files_version/" . $file_uri, array('Content-disposition' => "attachment; filename=$filename"));
}

/**
 * Function like file_transfer.
 *
 * @param string $uri
 *   File Uri.
 * @param string $headers
 *   Headers of file.
 */
function files_version_file_transfer($uri, $headers) {
  if (ob_get_level()) {
    ob_end_clean();
  }
  foreach ($headers as $name => $value) {
    drupal_add_http_header($name, $value);
  }
  drupal_send_headers();
  $scheme = file_uri_scheme($uri);
  // Transfer file in 1024 byte chunks to save memory usage.
  if ($scheme && file_stream_wrapper_valid_scheme($scheme) && $fd = fopen($uri, 'rb')) {
    while (!feof($fd)) {
      print fread($fd, 1024);
    }
    fclose($fd);
  }
  else {
    drupal_not_found();
  }
  file_unmanaged_delete_recursive("public://files_version/");
  drupal_exit();
}

/**
 * Check and give value of version required field.
 */
function files_version_get_version_to_save() {
  return (variable_get('version_required')) ? variable_get('version_required') : 2;
}

/**
 * Function display output of difference.
 *
 * @param string $from_version
 *   Version from you want to check difference.
 * @param string $base_name
 *   Base name of file.
 * @param string $current_version_file
 *   Path of current version file.
 * @param string $old_version_file
 *   Tempraory path of old version file.
 */
function files_version_output_file_difference($from_version, $base_name, $current_version_file, $old_version_file) {
  if ($from_version != "" && $base_name != "" && is_numeric($from_version)) {
    $base_file_level = t("Current Drupal instance file.");
    $version_file_level = t("Version of @fromversion File.", array("@fromversion" => $from_version));
    drupal_add_js(drupal_get_path('module', 'files_version') . '/js/sync.js', 'file');
    drupal_add_js('
    function diffUsingJS(viewType) {
      "use strict";
      var byId = function(id) {
        return document.getElementById(id);
      },
      base = difflib.stringAsLines(byId("baseText").value),
      newtxt = difflib.stringAsLines(byId("newText").value),
      sm = new difflib.SequenceMatcher(base, newtxt),
      opcodes = sm.get_opcodes(),
      diffoutputdiv = byId("diffoutput"),
      contextSize = byId("contextSize").value;
      diffoutputdiv.innerHTML = "";
      contextSize = contextSize || null;

      diffoutputdiv.appendChild(diffview.buildView({
        baseTextLines: base,
        newTextLines: newtxt,
        opcodes: opcodes,
        baseTextName: "' . $base_file_level . '",
        newTextName: "' . $version_file_level . '",
        contextSize: contextSize,
        viewType: viewType
      }));
    }', 'inline');
    $data['old_file'] = file_get_contents($old_version_file);
    $data['new_file'] = file_get_contents($current_version_file);
    $data['version'] = $from_version;
    $data['file_name'] = $base_name;
    return theme('files_folder_form', array('data' => $data));
  }
  else {
    drupal_set_message(t("Version @version or file name @filename are not valid", array("@version" => $from_version, "@filename" => $base_name)), "error");
    return MENU_ACCESS_DENIED;
  }
}
