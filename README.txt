
-- SUMMARY --

The files version module provide you functionality to check the version of
current Drupal7 instance. It iterate recursively and saves drupal files into
database and create a new version on based on current timestamp.

This module creates version of all non media files (mime type should be text/*)
examples
 1 *.php
 2 *.css
 3 *.js
 3 *.text
 4 *.inc etc.

Backup will be created in public://files_version directory. If once version.zip
is downloaded from programmatically via module then version immediately deleted
for security reasons. If you don't have zip extension then you have to delete
version folder manually.

-- REQUIREMENTS --

* This module require zip extension should be enable. If you will not enable zip
  extension then module will not able to download your created backup in form of
  zip.
  For more details for installation of zip extension please refer php.net

  http://php.net/manual/en/zip.installation.php

* This modules require libraries module which you can download from below url
  url: https://www.drupal.org/project/libraries
  download url: https://ftp.drupal.org/files/projects/libraries-7.x-2.3.tar.gz

* This module require "jsdifflib" library which you can download from
  https://github.com/cemerick/jsdifflib and put entire folder into
  site/all/libraies/ so your folder structure looks like.

  sites/all/libraries/jsdifflib
  sites/all/libraries/jsdifflib/difflib.js
  sites/all/libraries/jsdifflib/diffview.js
  sites/all/libraries/jsdifflib/diffview.css

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

* You likely want to disable Toolbar module, since its output clashes with
  Administration menu.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Use the administration pages and help (System module)

    The top-level administration categories require this permission to be
    accessible. The administration menu will be empty unless this permission is
    granted.

  - administer files version

    Users in roles with the "administer files version" permission will see
    the versions of files.   

* To download the backup go to Administration » Configuration » Files version
   
   Select the version for which you want create backup.

* To create new backup go to Administration » Configuration » Files version

   After pressing the Go button batch process will start. Please keep in mind do
   not close the browser till batch process finish. Otherwise some files may not
   save in database for version.

-- CUSTOMIZATION --

* Access menu from here : admin/config/filesversion

* Set the how much version you want to keep into database. Older entry will
  automatically deleted if you create new backup.
   
   admin/config/filesversion/files-version-settings

* Files version you can check from here 

  admin/config/filesversion/files-browser

-- CONTACT --

Current maintainers:
* Rajveer singh https://www.drupal.org/u/rajveergang
  email : rajveer.gang@gmail.com
